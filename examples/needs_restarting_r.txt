Core libraries or services have been updated:
  kernel -> 3.10.0-1062.4.3.el7
  systemd -> 219-67.el7_7.2

Reboot is required to ensure that your system benefits from these updates.

More information:
https://access.redhat.com/solutions/27943
