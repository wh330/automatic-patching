You should restart:
  * Some applications using:
      systemctl restart NetworkManager
      systemctl restart auditd
      systemctl restart crond
      systemctl restart goferd
      systemctl restart gssproxy
      systemctl restart httpd
      systemctl restart irqbalance
      systemctl restart lvm2-lvmetad
      systemctl restart postfix
      systemctl restart rhnsd
      systemctl restart rhsmcertd
      systemctl restart rsyslog
      systemctl restart sshd
      systemctl restart systemd-journald
      systemctl restart systemd-logind
      systemctl restart systemd-udevd
      systemctl restart tuned
      systemctl restart vgauthd
      systemctl restart vmtoolsd

  * These applications manually:
      pandora_agent

Additionally to those process above, there are:
  - 3 processes requiring restart of your session (i.e. Logging out & Logging in again)
  - 2 processes requiring reboot